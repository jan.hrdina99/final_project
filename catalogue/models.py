from django.db.models import Model, CharField, URLField, ForeignKey, FloatField, DateField, OneToOneField, IntegerField, \
    PositiveIntegerField
from django.contrib.auth.models import User


class Category(Model):
    category_name = CharField(max_length=64, blank=None, null=None)


class Product(Model):
    ''' ELECTRONIC = 'EC'
     PRODUCT_CHOICES = [
          (ELECTRONIC, 'Electronic'),
         ()] '''

    title = CharField(max_length=64, blank=False, null=None)
    description = CharField(max_length=1024, blank=False, null=None)
    thumbnail = URLField(blank=False, null=None)
    category = ForeignKey(Category, null=True, blank=False)
    price = FloatField(blank=False, null=None)
    ###product_type =
    author = ForeignKey(User, blank=False, null=None)
    product_category = ForeignKey(Category, blank=False, null=None)


class OrderLine(Model):
    product_rl = ForeignKey(Product, blank=False, null=False)
    num_of_products = PositiveIntegerField(blank=False, null=False)
    product_price = FloatField(blank=False, null=False)


class Order(Model):
    user_name = CharField(max_length=64, blank=False, null=False)
    total_cost = FloatField(blank=False, null=False)
    delivery_address = CharField(max_length=64, blank=False, null=False)
    user_address = CharField(max_length=64, blank=False, null=False)
    date_of_order = DateField(blank=False, null=False)
    order_lines = ForeignKey(OrderLine, blank=False, null=False)
    ###status = some enum?


class Cart(Model):
    order_lines = ForeignKey()
    #todo class cart_item()
    ### total_cost ?
